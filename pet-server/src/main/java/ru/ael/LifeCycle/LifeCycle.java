/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.LifeCycle;

import javax.inject.Inject;
import io.quarkus.runtime.StartupEvent;
import javax.enterprise.event.Observes;
import ru.ael.Entity.Pet;
import ru.ael.petservice.PetService;

/**
 *
 * @author student
 */
public class LifeCycle {

    @Inject
    PetService petService;

    void onStart(@Observes StartupEvent event) {

        Pet pet = new Pet();
        pet.setИмя("Шарик");
        petService.savePet(pet);
        System.out.println("Пытаемся сохранить животное");
        System.out.println("Сохраняет животное");
        System.out.println("Животное сохранилось");
    }

}
