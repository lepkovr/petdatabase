/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author pet
 */
public class Owner {
    private String имя;
    private String фамилия;
    private String отчество;
    private String возраст;

    @Id
    @SequenceGenerator(name = "petowmerSeq", sequenceName = "petowner_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "petownerSeq")
    private Long id;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    
    public String getИмя() {
        return имя;
    }

    public void setИмя(String имя) {
        this.имя = имя;
    }

    public String getФамилия() {
        return фамилия;
    }

    public void setФамилия(String фамилия) {
        this.фамилия = фамилия;
    }

    public String getОтчество() {
        return отчество;
    }

    public void setОтчество(String отчество) {
        this.отчество = отчество;
    }

    public String getВозраст() {
        return возраст;
    }

    public void setВозраст(String возраст) {
        this.возраст = возраст;
    }
}